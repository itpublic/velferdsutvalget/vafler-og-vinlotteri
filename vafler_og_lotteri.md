## Vafler
Vaflene koster 10 kr pr. plate, inkludert syltetøy og rømme/brunost.

### Fordeling av vaffelsteking

Vaffelsteking går på dugnad mellom seksjonene/administrasjonen, som fordeler stekingen internt. Følgende turnus gjelder:

| Måned (første halvår) | Ansvarlig enhet                      | Måned (andre halvår) | Ansvarlig enhet               |
|-----------------------|--------------------------------------|----------------------|-------------------------------|
| Januar                | IT Fellestjenester                   | *Juli*               | *ferietid*                    |
| Februar               | Prosjekt sikre forskningssystemer    | *August*             | *ferietid*                    |
| Mars                  | Digital administrasjon og formidling | September            | IT Fellestjenester            |
| April                 | IT Fellestjenester                   | Oktober              | Lærlinger                     |
| Mai                   | Digitalt læringsmiljø                | November             | Strategi og virksomhetsstøtte |
| Juni                  | Digitale forskningstjenester         | Desember             | IT Fellestjenester            |

(Administrasjonen og prosjektseksjonen har færre vakter enn resten pga. få personer å fordele på.)

2-3 personer trengs i tidsrommet ca. kl 9:30 - 11:30 til å handle inn, ordne til, steke og ordne opp. Se under.

### Innkjøp til vaffelsteking
Det som mangler handles og kvittering+vippskrav sendes til velferdsutvalget.

### Handeliste for vafler
(Sjekk status av kjølevarer og tørrvarer før innkjøp.)

| Handleliste                                                                                      |
|--------------------------------------------------------------------------------------------------|
| 12 (evt 10) egg, alt etter hvilken pakke som er tilgjengelig                                     |
| 2 liter kefir (evt surmelk hvis kefir ikke er tilgjengelig)                                      |
| 2 liter lettmelk/veldig lettmelk                                                                 |
| 2 kg hvetemel                                                                                    |
| En flaske flytende melange                                                                       |
| Sukker hvis dette ikke er tilgjengelig fra før. (det trengs ca 4-5 dl)                           |
| 2 bokser rømme, 1x brunost og jordbær- og bringebærsyltetøy (sjekk beholdning på forhånd)        |
| Normalt er det salt, vaniljesukker og bakepulver tilgjengelig, hvis ikke må dette også anskaffes |

**HUSK Å MERKE VARENE SOM SKAL I KJØLESKAPET MED "VELFERDSKOMITEEN"**

### Forberedelse til steking

Knus eggene og dump sukker i en bøtte. På dette tidspunktet var det bra at batteriene til drillen var godt oppladet på forhånd. Bland egg og sukker til noe som minner om eggedosis. Deretter blander du inn en snau flaske flytende margaring, 2 ts salt, 2 ts bakepulver og 4 ts vaniljesukker, og **gradvis** melk og mel (alt som ble kjøpt inn).

### Steking og servering

Det er lurt å handle på forhånd. (Se over.)

* Plasser vaffeljernene oppå (avis)papir for å unngå for mye griseri
* Sett på vaffeljernene. **Kursen skal tåle 2 doble vaffeljern, koblet opp i Sosial Sone 4.etg**
* Ordne til servering med vafler og tilbehør i Sosial Sone 4.etg.
* Etter stekingen: Rydd på kjøkkenet.
